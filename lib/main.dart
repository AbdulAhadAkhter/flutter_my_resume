import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'screens/work_experience.dart';
import 'screens/project_experience.dart';
import 'screens/social_media.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Resume',
      home: new Resume(),
    );
  }
}

class Resume extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 700,
        width: double.infinity,
        child: DefaultTabController(
            length: 3,
            child: Scaffold(
                bottomNavigationBar: BottomBar(),
                appBar: AppBar(
                  elevation: 0.0,
                  backgroundColor: Colors.transparent,
                  bottom: PreferredSize(
                    preferredSize: Size.fromHeight(55),
                    child: Container(
                      color: Colors.transparent,
                      child: SafeArea(
                        child: Column(
                          children: <Widget>[
                            TabBar(
                                indicator: UnderlineTabIndicator(
                                    borderSide: BorderSide(
                                        color: Colors.green, width: 4.0),
                                    insets: EdgeInsets.fromLTRB(
                                        40.0, 20.0, 40.0, 0)),
                                indicatorWeight: 15,
                                indicatorSize: TabBarIndicatorSize.label,
                                labelColor: Colors.green,
                                labelStyle: TextStyle(
                                    fontSize: 12,
                                    letterSpacing: 1.3,
                                    fontWeight: FontWeight.w500),
                                unselectedLabelColor: Colors.grey,
                                tabs: [
                                  Tab(
                                    text: "WORK",
                                    icon: Icon(Icons.book, size: 40),
                                  ),
                                  Tab(
                                    text: "PROJECTS",
                                    icon: Icon(Icons.build, size: 40),
                                  ),
                                  Tab(
                                    text: "SOCIAL MEDIA",
                                    icon: Icon(Icons.people, size: 40),
                                  ),
                                ])
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                body: TabBarView(
                  children: <Widget>[
                    Center(
                      child: WorkTab(),
                    ),
                    Center(
                      child: ProjectTab(),
                    ),
                    Center(
                      child: SocialTab(),
                    )
                  ],
                ))));
  }
}

class BottomBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(50, 0, 50, 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          FlatButton(
            child: Icon(Icons.phone),
            color: Colors.green,
            textColor: Colors.white,
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 25),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            onPressed: _launchPhone,
          ),
          CircleAvatar(
            radius: 45.0,
            backgroundImage: AssetImage('images/abdul.jpg'),
          ),
          FlatButton(
            child: Icon(Icons.email),
            color: Colors.green,
            textColor: Colors.white,
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 25),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            onPressed: _launchEmail,
          ),
        ],
      ),
    );
  }
}

_launchPhone() async {
  const url = 'tel:+12894892271';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

_launchEmail() async {
  const url = 'mailto:akhteraa@mcmaster.ca?subject=Hi';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
