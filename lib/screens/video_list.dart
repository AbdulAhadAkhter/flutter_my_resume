import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideoList extends StatefulWidget {
  @override
  _VideoListState createState() => _VideoListState();
}

class _VideoListState extends State<VideoList> {
  var videoIds = <String>[
    "o-qzH7WDKpk",
    "F2TqWB98nbk",
    "_-TGs3KAjrg",
    "Mrn9WW9Oqpk",
    "pTxSzJjQH_Y",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text("Video List"),
      ),
      body: ListView.separated(
        itemBuilder: (context, index) => YoutubePlayer(
              key: UniqueKey(),
              context: context,
              videoId: videoIds[index],
              flags: YoutubePlayerFlags(
                autoPlay: false,
                showVideoProgressIndicator: true,
                hideFullScreenButton: true,
              ),
            ),
        separatorBuilder: (_, i) => SizedBox(
              height: 10.0,
            ),
        itemCount: videoIds.length,
      ),
    );
  }
}
