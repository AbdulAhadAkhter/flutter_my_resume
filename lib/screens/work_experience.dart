import 'package:flutter/material.dart';

class WorkTab extends StatelessWidget {
  final Color _fontColor = Color(0xff5b6990);
  final double _smallFontSpacing = 1.3;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 25),
      alignment: Alignment.topCenter,
      child: ListView(
        children: <Widget>[
          Entry(
              fontColor: _fontColor,
              smallFontSpacing: _smallFontSpacing,
              company: "Self-employed - Freelancer",
              time: "07/18 - Now",
              description:
                  "●Worked on CG scenes for clients.\n●Aided in interior design processes\n●Built static websites for clients\n●Created YouTube channel focused on music fusion"),
          Entry(
              fontColor: _fontColor,
              smallFontSpacing: _smallFontSpacing,
              company: "Spotify - SDET & QA",
              time: "04/17 - 09/17",
              description:
                  "●Migrated webapps to new testing framework\n●Wrote internal test tooling apps\n●Fixed bugs on mobile\n●Logged bugs under testing sessions"),
          Entry(
              fontColor: _fontColor,
              smallFontSpacing: _smallFontSpacing,
              company: "Kobo Inc - QA Analyst",
              time: "05/16 - 04/17",
              description:
                  "●Spearheaded migration for Kotlin Automation\n●Maintained test suite for each release\n●Profiled defects for all platforms\n●Assisted with SEO Testing on all platforms"),
        ],
      ),
    );
  }
}

class Entry extends StatelessWidget {
  const Entry({
    Key key,
    @required Color fontColor,
    @required double smallFontSpacing,
    @required this.company,
    @required this.time,
    @required this.description,
  })  : _fontColor = fontColor,
        _smallFontSpacing = smallFontSpacing,
        super(key: key);

  final Color _fontColor;
  final double _smallFontSpacing;
  final String company;
  final String time;
  final String description;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
        color: Color(0xffdde9f7),
        width: 1.5,
      ))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                company,
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: _fontColor),
              ),
              SizedBox(width: 25.0),
              Text(
                time,
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: _fontColor),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            description,
            style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.w400,
                letterSpacing: _smallFontSpacing,
                color: _fontColor),
          )
        ],
      ),
    );
  }
}
