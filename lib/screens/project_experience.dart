import 'package:flutter/material.dart';

class ProjectTab extends StatelessWidget {
  final Color _fontColor = Color(0xff5b6990);
  final double _smallFontSpacing = 1.3;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 25),
      alignment: Alignment.topCenter,
      child: ListView(
        children: <Widget>[
          Entry(
              fontColor: _fontColor,
              smallFontSpacing: _smallFontSpacing,
              company: "Flutter Resume - Personal",
              technology: "Flutter/Dart",
              description:
                  "Personal learning project to play around with Flutter and Dart"),
          Entry(
              fontColor: _fontColor,
              smallFontSpacing: _smallFontSpacing,
              company: "Autonomous RC Car - Capstone",
              technology: "OpenCV/TF",
              description:
                  "Built autonomous car utilizing Nvidia Jetson TX2 assisted by machine learning to stay in lane and detect other objects(cars, stop signs, lanes)"),
          Entry(
              fontColor: _fontColor,
              smallFontSpacing: _smallFontSpacing,
              company: "NBody Simulation - Distributed",
              technology: "CUDA/C++",
              description:
                  "OpenMP and MPI accelerated N-Body simulation programming outputting simulations at 4K, running on SHARCNET cluster."),
          Entry(
              fontColor: _fontColor,
              smallFontSpacing: _smallFontSpacing,
              company: "Arcade Cabinet - Personal",
              technology: "RPi/Woodworking",
              description:
                  "Built a Raspberry Pi powered arcade cabinet using an MDF chassis and aftermarket arcade buttons and joysticks"),
          Entry(
              fontColor: _fontColor,
              smallFontSpacing: _smallFontSpacing,
              company: "BER Curves - Comm Systems",
              technology: "Matlab",
              description:
                  "Matlab Simulation showing the difference between various types of BERs."),
        ],
      ),
    );
  }
}

class Entry extends StatelessWidget {
  const Entry({
    Key key,
    @required Color fontColor,
    @required double smallFontSpacing,
    @required this.company,
    @required this.technology,
    @required this.description,
  })  : _fontColor = fontColor,
        _smallFontSpacing = smallFontSpacing,
        super(key: key);

  final Color _fontColor;
  final double _smallFontSpacing;
  final String company;
  final String technology;
  final String description;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
        color: Color(0xffdde9f7),
        width: 1.5,
      ))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                company,
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: _fontColor),
              ),
              SizedBox(width: 25.0),
              Text(
                technology,
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: _fontColor),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            description,
            style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.w400,
                letterSpacing: _smallFontSpacing,
                color: _fontColor),
          )
        ],
      ),
    );
  }
}
