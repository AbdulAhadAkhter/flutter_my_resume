import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'video_list.dart';

class SocialTab extends StatelessWidget {
  final Color _fontColor = Colors.white;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 25),
      alignment: Alignment.topCenter,
      child: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              onTap: () async {
                await Navigator.push(
                  context,
                  CupertinoPageRoute(builder: (context) => VideoList()),
                );
              },
              child: SizedBox(
                height: 50.0,
                width: double.infinity,
                child: Card(
                    elevation: 5.0,
                    color: Colors.green,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.videocam,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "YouTube.com/user/TheRealAbdulio",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                color: _fontColor),
                          ),
                        ])),
              ),
            ),
          ),
          SizedBox(height: 5),
        ],
      ),
    );
  }
}
